/*

  FUcking Segmented Input jquery plugin 1.0
  by Den (aolko)
  -*-
  Fuck you, telegram_js_cunts = ["jsChat","js_ru","js_noobs_ru","projs_ru","javascriptgroup"]
  
*/


(function( $ ) {
 
  $.fn.extend({
    fusi: function(argumentOptions) {
      var root = this;
      if($(" > *[id]",root)){var el = $(" > *[id]",root)}else{var el = $(" > *[input]",root)};
      var out = "";
      var keys = [8, 9, /*16, 17, 18,*/ 19, 20, 27, 33, 34, 35, 36, /*37,*/ 38, /*39,*/ 40, 45, 46, 106, 107, 109, 110, 111, 144, 145];

      el.attr({type:"number",inputmode:"numeric",pattern:"[0-9]*",value:"",maxlength:"1",placeholder:"0",max:"9",autocomplete:"off"});
      el.each(function(i,e){
        $(this).attr("aria-label","Number input #"+(i+1));
      });

      el.on("keydown",function(e){
        if (e.keyCode === 8 && $(this).val().length === 0) {
          if($(this).is(':first-child')){
            el.last().focus();
          } else {
            $(this).prev(el).focus();
          }
          // this should cover buttons keys blacklist
        } else if ($.inArray(e.keyCode, keys) >= 0) {
          return true;
          // this should cover addtl blacklisted button cases, not mentioned above
        } else if((e.keyCode >= 48 && e.keyCode <= 57) && (e.keyCode >= 96 && e.keyCode <= 105) && e.shiftKey) {
          return false;
        } else if (($(this).val().indexOf('.') !== -1) && (e.keyCode < 48 || e.keyCode > 57) && e.keyCode != 8){
          e.preventDefault();
          return false;
        }
        // arrow keys handling
        if (e.keyCode === 37){
          return false;
        } else if (e.keyCode === 39){
          return false;
        }
      }).on("keyup",function(e){
        // arrow keys handling
        if (e.keyCode === 37){
          if($(this).is(':first-child')){
            el.last().focus();
          } else {
            $(this).prev(el).focus();
          }
        } else if (e.keyCode === 39){
          if($(this).is(':last-child')){
            el.first().focus();
          } else {
            $(this).next(el).focus();
          }
        }
      });
      el.on("focus",function(){
        // select input value on focus
        $(this).select();
      });
      el.on("input",function (e) {
        // we can't just trust the blacklist, can we?
        e.target.value = e.target.value.replace(/[^0-9.]/g, '');
        $(this).attr("value",$(this).val());
        // out variable, returns the value from the control
        out = el.map(function(){return $( this ).attr("value");}).get().join( "" );
        root.value = out;
        //console.log(out);
        // input overflow prevention
        if ($(this).val().length >= $(this).attr("maxlength")) {
          if((e.shiftKey && e.Tab) || e.Tab){
            return false;
          } else if(e.shiftKey){
            return false;
          } else {
            if($(this).is(':last-child')){
              el.first().focus();
            } else {
              $(this).next(el).focus();
            }
            return false;
          }
        }
      });
      
      return this;
    }
  });
})( jQuery );
