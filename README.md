# FUSI

**FU**cking **S**egmented **I**nput. Useful for making segmented inputs, like pin code inputs, password inputs, or any other inputs that account for multiple inputs.

Supports smooth keyboard navigation (tab/shift+tab doesn't jump over inputs) and navigation via arrow keys.

Features looped navigation - focuses on the first input right after last one.

## Usage

```html
<script>
// define instance
var fusi = $("#element").fusi();
// get the value
console.log(fusi.value);
</script>
```

